import { useEffect, useState } from 'react';
import { Provider } from 'react-redux';
import { store } from './redux/store';
import { getCompany } from './api/company/apiHelper';


function App() {
  const [data, setData] = useState(null);

  console.log('getCompany data: ', data);


  useEffect(() => {
    const fetchDataCompany = async () => {
      const company = await getCompany();
      setData(company);
    }
    fetchDataCompany();
  }, []);

  return (
    <Provider store={store}>
      Hello Company
    </Provider>
  );
}

export default App;
