import { configureStore } from "@reduxjs/toolkit";
import companySlice from "./reducer/company-slice";

export const store = configureStore({
    reducer: {
        company: companySlice,
    }
});

