
import axios from 'axios';

export const getCompany = async () => {
    // axios.get('https://stockradars.co/assignment/data.php')
    //   .then(response => {
    //     return data = response;
    //   })
    //   .catch(error => {
    //     console.error(error);
    //     return data = error;
    //   });

    try {
        const res = await axios.get('https://stockradars.co/assignment/data.php');
        if (res?.status === 200) {
            const json = await res.data;
            return json;
        }
    } catch (error) {
        return error;
    }
}