import React, { useState } from 'react'
import {
    InputAdornment,
    TextField,
    createStyles,
    makeStyles
} from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import ClearIcon from "@material-ui/icons/Clear";
import debounce from "lodash/debounce";

  const useStyles = makeStyles(() => {
    return createStyles({
      search: {
        margin: "0"
      }
    });
  });

const Input = ({
    value,
    fncHandleChange,
    fncHandleClear
}) => {
    const { search } = useStyles();

    const [showClearIcon, setShowClearIcon] = useState("none");
    // const [value, setValue] = useState("");

    const handleChange = (event) => {
      setShowClearIcon(event.target.value === "" ? "none" : "flex");
      fncHandleChange(event.target.value);
    };
  
    const handleClear = () => {
      // TODO: Clear the search input
      setShowClearIcon("none");
      fncHandleClear();
    };

    const debouncedOnChange = debounce(handleChange, 500);

  return (
    <div className={search}>
        <TextField
          size="small"
          variant="outlined"
          value={value}
          onChange={debouncedOnChange}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SearchIcon />
              </InputAdornment>
            ),
            endAdornment: (
              <InputAdornment
                position="end"
                style={{ display: showClearIcon }}
                onClick={handleClear}
              >
                <ClearIcon />
              </InputAdornment>
            )
          }}
        />
    </div>
  )
}

export default Input;