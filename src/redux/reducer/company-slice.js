import { createSlice } from '@reduxjs/toolkit';


const initialState = {
    conpany: null,
  };

  const conpany = createSlice({
    name: 'conpany',
    initialState: initialState,
    reducers: {
        setConpany(state, action) {
            state.conpany = { ...state.conpany, ...action.payload };
        },
    },
  });

  export const { 
    setConpany,
  } = conpany.actions;

  export default conpany.reducer;